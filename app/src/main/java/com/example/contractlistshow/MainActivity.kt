package com.example.contractlistshow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.contractlistshow.dataclass.ContactPicker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_pick.setOnClickListener {
            val contactPicker: ContactPicker? = ContactPicker.create(
                activity = this,
                onContactPicked = { text.text = " ${it.number}" },
                onFailure = { text.text = it.localizedMessage })

            contactPicker?.pick() // call this to open the picker app chooser
        }

    }
}